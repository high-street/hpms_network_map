// Author: Rob Owens (owens@highstreetconsulting.com)

// var mbAttr = 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
//              '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
//              'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
//     mbUrl = 'https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw';
//
// var grayscale = L.tileLayer(mbUrl, {id: 'MapID', tileSize: 512, zoomOffset: -1, attribution: mbAttr}),
//     streets   = L.tileLayer(mbUrl, {id: 'MapID', tileSize: 512, zoomOffset: -1, attribution: mbAttr});

// HepGIS
// View/Filter color by functional class
// View/Filter color by ownership
// View/Filter weight by traffic volumes
// Filters
// Percent Change of ADT from one year from the next

var map = L.map('hpms_map', {
    center: [39.73, -104.99],
    zoomDelta: 0.5,
    zoomSnap: 0.5,
    zoom: 10
})

L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1Ijoicm9iZXJ0bG93ZW5zMjY2IiwiYSI6ImNraHV5YmllODBic2sycW1ubWEzdjRyOHcifQ.XccAQN73HnYM1kilLgfTDQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 15,
    id: 'mapbox/streets-v11',
    accessToken: 'pk.eyJ1Ijoicm9iZXJ0bG93ZW5zMjY2IiwiYSI6ImNraHV5YmllODBic2sycW1ubWEzdjRyOHcifQ.XccAQN73HnYM1kilLgfTDQ'
}).addTo(map);
// mapbox://styles/robertlowens266/cki6esdg7739119p5sjblpml4
L.control.scale().addTo(map);

const stateAbbreviations = [
    // 'AL','AK','AS','AZ','AR','CA','CO','CT','DE','DC','FM','FL','GA',
    // 'GU','HI','ID','IL','IN','IA','KS','KY','LA','ME','MH','MD','MA',
    // 'MI','MN','MS','MO','MT','NE','NV','NH','NJ','NM','NY','NC','ND',
    // 'MP','OH','OK','OR','PW','PA','PR','RI','SC','SD','TN','TX','UT',
    // 'VT','VI','VA','WA','WV','WI',
    'CO', 'WY'
];

var layers = []
for (state in stateAbbreviations) {
    state = stateAbbreviations[state]
    var url = 'https://geo.dot.gov/server/rest/services/Hosted/HPMS_' + state + '_GeoIntersections_2019/FeatureServer/0/'
    var layer = L.esri.featureLayer({
        url: url,
        where: "f_system = 1"
    }).addTo(map)
    layers.push(layer)
}

map.on('zoomend', function() {
    zoom = map.getZoom()
    console.log(zoom)
    var highest_f_class = 1
    if (zoom > 8)  { highest_f_class = 2 }
    if (zoom > 10) { highest_f_class = 3 }
    if (zoom > 11) { highest_f_class = 4 }
    if (zoom > 12) { highest_f_class = 5 }
    if (zoom > 13) { highest_f_class = 6 }
    if (zoom > 14) { highest_f_class = 7 }
    for (layer in layers) {
        layer = layers[layer]
        layer.setWhere("f_system <=" + highest_f_class)
    }
});
