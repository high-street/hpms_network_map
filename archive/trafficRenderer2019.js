      var trafficRenderer2019 = {
        type: "simple",
        symbol: {
          color: "#3449eb",
          type: "simple-line",
          style: "solid"
        },
        visualVariables: [
          {
            type: "size",
            field: "aadt",
            minDataValue: 5000,
            maxDataValue: 250000,
            minSize: "0px",
            maxSize: "12px"
          }
        ]
      };