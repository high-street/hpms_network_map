      var trafficRenderer2018 = {
        type: "simple",
        symbol: {
          color: "#e63030",
          type: "simple-line",
          style: "solid"
        },
        visualVariables: [
          {
            type: "size",
            field: "aadt",
            minDataValue: 5000,
            maxDataValue: 250000,
            minSize: "0px",
            maxSize: "12px"
          }
        ]
      };