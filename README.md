# DVC HPMS Network Maps

## Publishing to Apps.highstreetconsulting.com

1. Open Up FileZilla
2. Open up File Manager
3. Connect to apps.highstreetconsulting.com using robo username.
4. Password saved in FileZilla... if not... who knows?
5. Go to /var/www/html/hpms_network_maps in the server
6. Upload new files
7. Voila!

http://apps.highstreetconsulting.com/hpms_network_maps/
