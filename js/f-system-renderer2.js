////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////
// Author: Rob Owens (Owens@highstreetconsulting.com)
// Date: 12/27/2020
/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const fSystemRenderer2 = {
    type: "unique-value",
    field: "f_system",
    defaultSymbol: { type: "simple-line", size: "4px" },
    legendOptions: { title: "Functional Class" },
    uniqueValueInfos: [
        {
            value: 1,
            symbol: {
                type: "simple-line",
                color: [0, 112, 255]
            }
        }, {
            value: 2,
            symbol: {
                type: "simple-line",
                color: [189, 126, 190]
            }
        }, {
            value: 3,
            symbol: {
                type: "simple-line",
                color: [255, 0, 0]
            }
        }, {
            value: 4,
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 5,
            symbol: {
                type: "simple-line",
                color: [230, 152, 0]
            }
        }, {
            value: 6,
            symbol: {
                type: "simple-line",
                color: [56, 168, 0]
            }
        }, {
            value: 7,
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }],
    visualVariables: [
        {
            type: "size",
            field: "aadt",
            minDataValue: 5000,
            maxDataValue: 250000,
            minSize: "1px",
            maxSize: "12px",
            legendOptions: {
                title: "Annual Average Daily Traffic"
            }
        }
    ]
};
