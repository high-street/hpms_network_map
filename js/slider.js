const max = 250000
const min = 0
var adt1 = min
var adt2 = max

$(function() {
    $("#slider-range").slider({
        range: true,
        min: min,
        max: max,
        values: [min, max],
        step: 1000,
        slide: function(event, ui) {
            var maxLabel = ""
            if (ui.values[1] == max) {
                maxLabel = ui.values[1] + "+"
            } else {
                maxLabel = ui.values[1]
            }
            $("#amount").val(ui.values[0] + " - " + maxLabel);
        },
        change: function(event, ui) {
            adt1 = ui.values[0]
            adt2 = ui.values[1]
        }
    });

    var maxLabel = ""
    if ($("#slider-range").slider("values", 1) == max) {
        maxLabel = $("#slider-range").slider("values", 1) + "+"
    } else {
        maxLabel = $("#slider-range").slider("values", 1)
    }

    $("#amount").val($("#slider-range").slider("values", 0) + " - " + maxLabel);
  });
