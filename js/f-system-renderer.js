////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////
// Author: Rob Owens (Owens@highstreetconsulting.com)
// Date: 12/27/2020
/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const fSystemRenderer = {
    type: "unique-value",
    field: "f_system",
    defaultSymbol: { type: "simple-line", size: "4px" },
    legendOptions: { title: "Functional Class" },
    uniqueValueInfos: [
        {
            value: 1,
            label: "Interstate",
            symbol: {
                type: "simple-line",
                color: [0, 112, 255],
                width: "7px"
            }
        }, {
            value: 2,
            label: "Principal Arterial – Other Freeways and Expressways",
            symbol: {
                type: "simple-line",
                color: [189, 126, 190],
                width: "6px"
            }
        }, {
            value: 3,
            label: "Principal Arterial – Other",
            symbol: {
                type: "simple-line",
                color: [255, 0, 0],
                width: "5px"
            }
        }, {
            value: 4,
            label: "Minor Arterial",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0],
                width: "4px"
            }
        }, {
            value: 5,
            label: "Major Collector",
            symbol: {
                type: "simple-line",
                color: [230, 152, 0],
                width: "3px"
            }
        }, {
            value: 6,
            label: "Minor Collector",
            symbol: {
                type: "simple-line",
                color: [56, 168, 0],
                width: "2px"
            }
        }, {
            value: 7,
            label: "Local",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178],
                width: "1px"
            }
        }]
};
