const good = {
  type: "simple-line",
  color: "#69d84f"
};

const fair = {
  type: "simple-line",
  color: "#ffcc00"
};

const poor = {
  type: "simple-line",
  color: "#ff8080"
};

var iriRenderer = {
    type: "class-breaks",
    field: "IRI",
    defaultSymbol: { type: "simple-line", width: "4px", color:  [128, 128, 128, .5] },
    legendOptions: { title: "International Roughness Index" },
    defaultLabel: "No Data",
    classBreakInfos: [
        {
            minValue: 0,
            maxValue: 95,
            symbol: good,
            label: "Good"
        },
        {
            minValue: 91,
            maxValue: 170,
            symbol: fair,
            label: "Fair"
        },
        {
            minValue: 170,
            maxValue: 9999,
            symbol: poor,
            label: "Poor"
        }
    ],
    visualVariables: [
        {
            type: "size",
            field: "aadt",
            minDataValue: 5000,
            maxDataValue: 250000,
            minSize: "1px",
            maxSize: "12px",
            legendOptions: {
                title: "Annual Average Daily Traffic"
            }
        }
    ]
};
