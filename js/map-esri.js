////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////
// Author: Rob Owens (Owens@highstreetconsulting.com)
// Date: 12/27/2020
/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const dataYear = 2018
var ownerWhere = ""
var adtWhere = ""
var fClassWhere = ""

require([
    "esri/Map",
    "esri/views/MapView",
    "esri/widgets/BasemapToggle",
    "esri/layers/FeatureLayer",
    "esri/widgets/Search",
    "esri/core/watchUtils",
    "esri/widgets/Legend",
    "esri/widgets/Expand",
    "esri/tasks/support/Query"
], function(Map, MapView, BasemapToggle, FeatureLayer, Search, watchUtils,
    Legend, Expand, Query) {

    function filterLayers() {

        where = fClassWhere + ownerWhere + adtWhere

        for (layer in layers) {
            view.whenLayerView(layers[layer]).then(function (featureLayerView){
                featureLayerView.filter = {
                    where: where
                }
            })
        }

    }

    var map = new Map({
        basemap: "topo-vector"
    });

    var view = new MapView({
        container: "hpms_map",
        map: map,
        center: [-77.002857, 38.875859],
        zoom: 17
    });

    // map widgets and basemaps
    var basemapToggle = new BasemapToggle({
        view: view,
        secondMap: "satellite"
    });

    var search = new Search({
        view: view
    });

    var legend = new Legend({
        view: view
    });


    // DELETE???

    // var usdot_feature = [
    //  {
    //    geometry: {
    //      type: "point",
    //      x: -77.002857,
    //      y: 38.875859
    //    },
    //    attributes: {
    //      ObjectID: 1,
    //      MsgTime: Date.now(),
    //      name: "US Department of Transportation"
    //    }
    //  }
    // ];

    // var usdot_layer = new FeatureLayer({
    //     source: usdot_feature,  // autocast as a Collection of new Graphic()
    //     objectIdField: "ObjectID",
    //     legendEnabled: false,
    //     renderer: {
    //         type: "simple",
    //         symbol: {
    //             type: "picture-marker",
    //             url: "css/us_dot_logo.png",
    //             width: "250px",
    //             height: "60px"
    //         }
    //     }
    // });
    // map.add(usdot_layer)

    // DELETE???

    // hard code menu locations
    view.ui.add(basemapToggle,"bottom-right");
    view.ui.add(search, "top-right");
    view.ui.add(legend, "bottom-left");

    var highest_f_class = 1

    // ALASKA not working?
    const stateNames = [
        { state: 'Alabama', region: "South East" },
        { state: 'Arizona', region: "South West" },
        { state: 'Arkansas', region: "South East" },
        { state: 'California', region: "West Coast" },
        { state: 'Colorado', region: "Mountain West" },
        { state: 'Connecticut', region: "Mid-Atlantic" },
        { state: 'Delaware', region: "Mid-Atlantic" },
        { state: 'District', region: "Mid-Atlantic" },
        { state: 'Florida', region: "South East" },
        { state: 'Georgia', region: "South East" },
        { state: 'Hawaii', region: "?" },
        { state: 'Idaho', region: "Mountain West" },
        { state: 'Illinois', region: "Midwest" },
        { state: 'Iowa', region: "Midwest" },
        { state: 'Kansas', region: "Midwest" },
        { state: 'Kentucky', region: "Mid-Atlantic" },
        { state: 'Louisiana', region: "South East" },
        { state: 'Maine', region: "New England" },
        { state: 'Maryland', region: "Mid-Atlantic" },
        { state: 'Massachusetts', region: "New England" },
        { state: 'Michigan', region: "Midwest" },
        { state: 'Minnesota', region: "Midwest" },
        { state: 'Mississippi', region: "South East" },
        { state: 'Missouri', region: "Midwest" },
        { state: 'Montana', region: "Mountain West" },
        { state: 'Nebraska', region: "Midwest" },
        { state: 'Nevada', region: "South West" },
        { state: 'NewHampshire', region: "New England" },
        { state: 'NewJersey', region: "Mid-Atlantic" },
        { state: 'NewMexico', region: "South West" },
        { state: 'NewYork', region: "Mid-Atlantic" },
        { state: 'NorthCarolina', region: "Mid-Atlantic" },
        { state: 'NorthDakota', region: "Midwest" },
        { state: 'Ohio', region: "Midwest" },
        { state: 'Oklahoma', region: "?" },
        { state: 'Oregon', region: "West Coast" },
        { state: 'Pennsylvania', region: "Mid-Atlantic" },
        { state: 'PuertoRico', region: "South East" },
        { state: 'RhodeIsland', region: "New England" },
        { state: 'SouthCarolina', region: "South East" },
        { state: 'SouthDakota', region: "Midwest" },
        { state: 'Tennessee', region: "South East" },
        { state: 'Texas', region: "South West" },
        { state: 'Utah', region: "Mountain West" },
        { state: 'Vermont', region: "New England" },
        { state: 'Virginia', region: "Mid Atlantic" },
        { state: 'Washington', region: "West Coast" },
        { state: 'WestVirginia', region: "Mid-Atlantic" },
        { state: 'Wisconsin', region: "Midwest" },
        { state: 'Wyoming', region: "Mountain West" }
    ]

    var layers = []

    for (state in stateNames) {
        stateName = stateNames[state]["state"]
        if (state == 0) {
            legendOn = true
        } else { legendOn = false }

        const server = "https://geo.dot.gov/server/rest/services/Hosted/"
        const url = server + stateName + "_" + dataYear + "_PR/FeatureServer"

        var newLayer = new FeatureLayer({
            url: url,
            id: stateName,
            outFields: HPMSattributes,
            popupTemplate: popupRoadAttributes,
            returnM: false,
            returnZ: false,
            renderer: fSystemRenderer,
            legendEnabled: legendOn,
            title: "Legend",
            minScale: 300000,
            cap: "butt"
        })

        layers.push(newLayer)
        map.add(newLayer)

        // Init View Filtered to F-System of 1
        // view.whenLayerView(newLayer).then(function (featureLayerView){
        //     featureLayerView.filter = {
        //         where: "f_system = 1"
        //     }
        // })
    }

    // Add in dropdown elements
    view.whenLayerView(layers[0]).then(function (layerView) {

        const viewMapBy = new Expand({
            view: view,
            content: viewMapElement,
            expandIconClass: "esri-icon-maps",
            group: "top-left"
        });

        // //clear the filters when user closes the expand widget
        // functionalClassFilter.watch("expanded", function () {
        //   if (!functionalClassFilter.expanded) {
        //     layerView.filter = null;
        //   }
        // });

        view.ui.add(viewMapBy, "top-left");

    });

    // // Populate Owner Field
    // owners = []
    // for (layer in layers) {
    //     var query = layers[layer].createQuery()
    //     query.returnDistinctValues = true
    //     query.outFields = ["ownership"];
    //     layers[layer].queryFeatures(query).then(function(response) {
    //         for (feature in response.features) {
    //             owner = response.features[feature].attributes.ownership
    //             console.log(owner)
    //             if (owner != null) {
    //                 owners.push(owner)
    //             }
    //         }
    //     })
    // }

    // Filter Map to Current Zoom
    watchUtils.when(view, "interacting", function() {
    // watchUtils.when(view, "stationary", function() {

        const zoom = view.get('zoom');

        if (zoom < 16) {
            map.remove(usdot_layer)
        }

        if ($("#f-class").val() == 0) {

            // if (zoom > 11)  { highest_f_class = 2 }
            // if (zoom > 12)  { highest_f_class = 3 }
            // if (zoom > 13) { highest_f_class = 4 }
            // if (zoom > 14) { highest_f_class = 5 }
            // if (zoom > 15) { highest_f_class = 6 }
            // if (zoom > 16) { highest_f_class = 7 }
            // filterLayers()

        }

    });

    const viewMapElement = document.getElementById("view-map");


    // viewMapElement.addEventListener("click", switchRenderer);
    $(".symb-btn").on("click change select", function() {

        console.log('BTN')

        renderer_id = $(this).val();

        console.log(renderer_id)

        var renderer;
        switch(renderer_id) {
            case "a":
                renderer = fSystemRenderer;
                break;
            case "b":
                renderer = fSystemRenderer2;
                break;
            case "c":
                renderer = ownerRenderer;
                break;
            case "d":
                renderer = iriRenderer;
                break;
        }

        for (layer in layers) {
            layers[layer].renderer = renderer
        }
    })

    $("#f-class").change(function(){
        fClassWhere = "f_system = " + $("#f-class").val();
        if ($("#f-class").val() == 0) {
            fClassWhere = "1=1";
        }
        filterLayers()
    })

    $("#owner").change(function(){

        ownerWhere = "OWNERSHIP =" + $("#owner").val() + " AND "
        if ($("#owner").val() == "All") {
            ownerWhere = ""
        }

        filterLayers()
    })

    $("#slider-range").on("slidestop", function(event, ui) {

        var adt_max_filter = ui.values[1]
        if (ui.values[1] == max) {
            adt_max_filter = 999999999
        }

        adtWhere = "aadt >=" + ui.values[0] + " AND aadt <= " + adt_max_filter

        filterLayers()

    })

    // loading modal
    $('.modal').show();
    function check_load(){
        /*
        Check to see if the view.updating === true then close modal
        */
        if (view.updating === false){
            clearInterval(interval_test);
            $('.modal-title').empty().text('Loading Complete');
            $('.modal').fadeOut(3000)
        }
    }
    var interval_test = setInterval(check_load, 1000);

});
