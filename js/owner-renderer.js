////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////
// Author: Rob Owens (Owens@highstreetconsulting.com)
// Date: 12/27/2020
/////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

const ownerRenderer = {
    type: "unique-value",
    field: "ownership",
    defaultSymbol: { type: "simple-line", weight: "4px"},
    legendOptions: { title: "Owner" },
    uniqueValueInfos: [
        {
            value: 1,
            label: "State Highway Agency",
            symbol: {
                type: "simple-line",
                color: [0, 112, 255]
            }
        }, {
            value: 2,
            label: "County Highway Agency",
            symbol: {
                type: "simple-line",
                color: [189, 126, 190]
            }
        }, {
            value: 3,
            label: "Town or Township Highway Agency",
            symbol: {
                type: "simple-line",
                color: [255, 0, 0]
            }
        }, {
            value: 4,
            label: "City or Municipal Highway Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 11,
            label: "State Park, Forest, or Reservation Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 12,
            label: "Local Park, Forest or Reservation Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 21,
            label: "Other State Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 25,
            label: "Other Local Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 26,
            label: "Private (other than Railroad)",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 27,
            label: "Railroad",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 31,
            label: "State Toll Road",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 32,
            label: "Local Toll Authority",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 40,
            label: "Other Public Instrumentality (i.e., Airport)",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 50,
            label: "Indian Tribe Nation",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 60,
            label: "Other Federal Agency",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 62,
            label: "Bureau of Indian Affairs",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 63,
            label: "Bureau of Fish and Wildlife",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 64,
            label: "U.S. Forest Service",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 66,
            label: "National Park Service",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        },  {
            value: 67,
            label: "Tennessee Valley Authority",
            symbol: {
                type: "simple-line",
                color: [168, 56, 0]
            }
        }, {
            value: 68,
            label: "Bureau of Land Management",
            symbol: {
                type: "simple-line",
                color: [230, 152, 0]
            }
        }, {
            value: 69,
            label: "Bureau of Reclamation",
            symbol: {
                type: "simple-line",
                color: [56, 168, 0]
            }
        }, {
            value: 70,
            label: "Corps of Engineers",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }, {
            value: 72,
            label: "Air Force",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }, {
            value: 73,
            label: "Navy/Marines",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }, {
            value: 74,
            label: "Army",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }, {
            value: 80,
            label: "Other",
            symbol: {
                type: "simple-line",
                color: [178, 178, 178]
            }
        }],
        visualVariables: [
            {
                type: "size",
                field: "aadt",
                minDataValue: 5000,
                maxDataValue: 250000,
                minSize: "1px",
                maxSize: "12px",
                legendOptions: {
                    title: "Annual Average Daily Traffic"
                }
            }
        ]
};
